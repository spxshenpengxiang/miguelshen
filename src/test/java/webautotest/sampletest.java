package webautotest;
import java.io.IOException;
import java.net.*;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
public class sampletest{
	
	public WebDriver driver;
	public String projectpath = System.getProperty("user.dir");
	public String baseurl="http://17.87.100.240/fmi/webd/MasterDemo_App?script=Automator%20-%20Worker%20Main";

    @BeforeTest
	public void launchbrowser()
	{	
    	System.out.println("Java Application:" + projectpath+"/chromedriver");
		System.out.println("This is the launching phase");
		System.setProperty("webdriver.chrome.driver", projectpath+"/chromedriver");
		//System.getProperty("webdriver.chrome.driver", "chromedrive");
		
		
	}
    @Test(invocationCount = 2)
    public void webcheck() throws IOException 
	{	
    	
    	driver = new ChromeDriver();
    	driver.get(baseurl);
    	String actualtitle = driver.getTitle();
		System.out.println(actualtitle);
    	URL url = new URL(baseurl);
    	System.out.println(url);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        System.out.println("Response code of the object is "+code);
        if (code==200)
        {
            System.out.println("It is working OK");
        }
	}
    @AfterTest
	public void terminatebrowser()
	{	
		//driver.close();
		System.out.println("Test is keep running on");
		
	}

	
}
