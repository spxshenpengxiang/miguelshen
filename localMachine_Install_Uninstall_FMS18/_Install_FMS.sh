#!/bin/bash


script_start_time=`date +%s`

#Change directory to the same path of this script, so that the script is independent on where it's run from
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

#Set variables
./Set_variables.sh

#Install FileMaker Server on the VM created
./Silent_Install_FMS.sh

#Test if FileMaker Server is installed correctly by checking /fmws/serverinfo
InstalledServerBuild=`cat var_FMS_latest_build.txt`
InstalledServerVersion=`cat var_FMS_version.txt`
InstalledHostAddress=`cat var_Host_Address.txt`

ActualServerBuild=`curl -k https://${InstalledHostAddress}/fmws/serverinfo | grep ServerVersion`

#To check whether "SPACE+BUILDNUMBER+( exist in ActualServerBuild retrieved from /fmws/serverinfo"
CheckString=" "$InstalledServerBuild"("
if [[ $ActualServerBuild = *$CheckString* ]];then
	echo ████████████████████████████████████████████████████████████████████████████
	echo FileMaker Server $InstalledServerVersion x$InstalledServerBuild is successfully installed on ${InstalledHostAddress}.
	echo ████████████████████████████████████████████████████████████████████████████
fi

#Enable WPE and WebD through FAC.
python EnableWebD.py

#Remove temp files
rm var_*.txt
rm *.pkg
rm *.fmcert
rm Assisted\ Install.txt

#Stop & start FMS processes to make custom cert working
echo 'FileMaker123'| sudo -S command
sudo sudo launchctl stop com.filemaker.fms
sleep 40
sudo sudo launchctl start com.filemaker.fms
sleep 5

#Calculate and display script run time
script_end_time=`date +%s`
runtime=$((script_end_time-script_start_time))
echo Script compeleted in $runtime seconds.
