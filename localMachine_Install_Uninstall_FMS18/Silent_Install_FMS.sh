#!/bin/bash

#Change directory to the same path of this script, so that the script is independent on where it's run from
script_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$script_path"

#Set variables from values saved in files.
file1=$(<var_Host_Address.txt)
#echo $file1

file2=$(<var_Host_Password.txt)
#echo $file2

#Set FMS version number
file3=$(<var_FMS_version.txt)
#echo $file3

#Set FMS build number
file4=$(<var_FMS_latest_build.txt)
#echo $file4

#Result Example: "http://scm-san.filemaker.com/neon/Releases/FMS/17_Current/107/Mac_Installer_Server.zip"
path1=http://scm-san.filemaker.com/neon/Releases/FMS/
#To install PostGM version, modify the following path to be path2=_PostGM/
path2=_PostGM/
path3=/Mac_Installer_Server.zip
para_build_URL=${path1}$file3${path2}$file4${path3}
#echo $para_build_URL

#Result Example: "MODURMYKTYOE\r"
para_password=$var_Host_Password
#echo $para_password

#Result Example: "'FileMaker Server 18.pkg'"
part1='FileMaker Server '
para_package_name="$part1$file3.pkg"
#echo $para_package_name

#verify if the installer package in the current directory
if [ -e 'Mac_Installer_Server.zip' ]
    #Unzip FMS installer
    then unzip -o 'Mac_Installer_Server.zip'
    sleep 1
else
#Download & unzip the FMS installer
    curl -O $para_build_URL
    sleep 1
    unzip -o 'Mac_Installer_Server.zip'
    sleep 1
fi

#Delete orignal Assisted Install.txt
rm 'Assisted Install.txt'

#Copy the prepared Assisted Install.txt from local folder to remote host.
cp -fr 'Assisted_Install.txt' 'Assisted Install.txt'

#Run silent install
echo 'FileMaker123'| sudo -S command
sudo installer -pkg "$para_package_name" -target /
sleep 1

#append printresult "FileMaker Server " $var_FMS_version " x" $var_FMS_build " is successfully installed on " $var_Host_Address "."
#puts $printresult
