#!/bin/bash

#Change directory to the same path of this script, so that the script is independent on where it's run from
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

#Open local accounts and password
var_local_Password="FileMaker123"

#Set current FMS version
var_FMS_version="19"
echo -n $var_FMS_version > var_FMS_version.txt

#Get latest FMS build
FMS_latest_build=`curl http://scm-san.filemaker.com/neon/Releases/FMS/mac_latest_build.txt | sed -n 3p | tr -d 'x' | tr -d '\n\r'`

########################################################################
#Enable below line and replace xxx with a build number to install a specific build number, this is to overwrite default behavior of installing latest build. 
#FMS_latest_build="34"
########################################################################
echo -n $FMS_latest_build > var_FMS_latest_build.txt

#Set local address
host_Address=127.0.0.1
echo -n $host_Address > var_Host_Address.txt

#Get password
Host_Password=$var_local_Password
echo -n $Host_Password > var_Host_Password.txt

