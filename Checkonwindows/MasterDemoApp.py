import webbrowser
import sys
import threading
import time
test_url = 'http://127.0.0.1/fmi/webd/MasterDemo_App?script=Automator%20-%20Worker%20Main'
def background_calculation():
    webbrowser.open(test_url)
def time_execution(count):
    i = 0
    while i < int(count):
        i = i + 1
        print(f"Now the {i} tab is working on {test_url}")
        background_calculation()
        time.sleep(2)
if __name__ == '__main__':
    count = sys.argv[1]
    t_process = threading.Thread(target=time_execution(count), name='LoopThread')
    t_process.start()